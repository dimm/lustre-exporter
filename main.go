package main

import (
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"io/ioutil"
	"log"
	"net"
	"net/http"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"
)

var (
	OpenGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_open_number",
		Help: "Number of open",
	},
		[]string{"server", "client"})
	CloseGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_close_number",
		Help: "Number of close",
	},
		[]string{"server", "client"})
	MknodGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_mknod_number",
		Help: "Number of mknod",
	},
		[]string{"server", "client"})
	LinkGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_link_number",
		Help: "Number of link",
	},
		[]string{"server", "client"})
	UnlinkGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_unlink_number",
		Help: "Number of unlink",
	},
		[]string{"server", "client"})
	MkdirGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_mkdir_number",
		Help: "Number of mkdir",
	},
		[]string{"server", "client"})
	RmdirGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_rmdir_number",
		Help: "Number of rmdir",
	},
		[]string{"server", "client"})
	RenameGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_rename_number",
		Help: "Number of rename",
	},
		[]string{"server", "client"})
	GetattrGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_getattr_number",
		Help: "Number of getattr",
	},
		[]string{"server", "client"})
	SetattrGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_setattr_number",
		Help: "Number of setattr",
	},
		[]string{"server", "client"})
	GetxattrGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_getxattr_number",
		Help: "Number of getxattr",
	},
		[]string{"server", "client"})
	SetxattrGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_setxattr_number",
		Help: "Number of setxattr",
	},
		[]string{"server", "client"})
	StatfsGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_statfs_number",
		Help: "Number of statfs",
	},
		[]string{"server", "client"})
	SyncGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_sync_number",
		Help: "Number of sync",
	},
		[]string{"server", "client"})
	SamedirRenameGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_samedirrename_number",
		Help: "Number of samedirrename",
	},
		[]string{"server", "client"})
	CrossdirRenameGauge = prometheus.NewGaugeVec(prometheus.GaugeOpts{
		Name: "lustre_crossdirrename_number",
		Help: "Number of crossdirrename",
	},
		[]string{"server", "client"})
)

func main() {
	prometheus.MustRegister(OpenGauge)
	prometheus.MustRegister(CloseGauge)
	prometheus.MustRegister(MknodGauge)
	prometheus.MustRegister(LinkGauge)
	prometheus.MustRegister(UnlinkGauge)
	prometheus.MustRegister(MkdirGauge)
	prometheus.MustRegister(RmdirGauge)
	prometheus.MustRegister(RenameGauge)
	prometheus.MustRegister(GetattrGauge)
	prometheus.MustRegister(SetattrGauge)
	prometheus.MustRegister(GetxattrGauge)
	prometheus.MustRegister(SetxattrGauge)
	prometheus.MustRegister(StatfsGauge)
	prometheus.MustRegister(SyncGauge)
	prometheus.MustRegister(SamedirRenameGauge)
	prometheus.MustRegister(CrossdirRenameGauge)

	hostname, _ := os.Hostname()

	go func() {
		for range time.NewTicker(time.Duration(15) * time.Second).C {
			if clients, err := filepath.Glob("/proc/fs/lustre/mdt/*/exports/*/stats"); err != nil {
				log.Printf("%s", err.Error())
			} else {
				for _, stats := range clients {
					if b, err := ioutil.ReadFile(stats); err != nil {
						log.Printf("%s", err.Error())
					} else {
						for _, counter := range strings.Split(fmt.Sprintf("%s", b), "\n") {
							count := strings.Fields(counter)
							if len(count) > 2 {
								ip := strings.Split(filepath.Base(filepath.Dir(stats)), "@")[0]
								addr := ip
								if host, err := net.LookupAddr(addr); err == nil && len(host) > 0 {
									addr = host[0]
								}

								switch count[0] {
								case "open":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									OpenGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "close":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									CloseGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "mknod":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									MknodGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "link":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									LinkGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "unlink":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									UnlinkGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "mkdir":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									MkdirGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "rmdir":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									RmdirGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "rename":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									RenameGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "getattr":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									GetattrGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "setattr":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									SetattrGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "getxattr":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									GetxattrGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "setxattr":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									SetxattrGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "statfs":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									StatfsGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "sync":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									SyncGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "samedirrename":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									SamedirRenameGauge.WithLabelValues(hostname, addr).Set(float64(value))
								case "crossdirrename":
									value, _ := strconv.ParseInt(count[1], 10, 0)
									CrossdirRenameGauge.WithLabelValues(hostname, addr).Set(float64(value))
								}
							}
						}
					}
				}
			}
		}
	}()

	http.Handle("/metrics", promhttp.Handler())
	log.Fatal(http.ListenAndServe(":9116", nil))
}
